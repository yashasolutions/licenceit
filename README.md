# licence it

 A CLI to generate licences files

## Install

```sh
# Using Yarn
yarn global add  licenceit

# Using npm
npm install -g licenceit
```

## Usage

```sh
licenceit
```

## Author

👤 **YS**

* Twitter: [@yashasolutions](https://twitter.com/yashasolutions)
* Web: [yasha.solutions](https://yasha.solutions)

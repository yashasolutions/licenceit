#! /usr/bin/env node

const prompts = require('prompts');

const {
  getLicencesNames, getFileType, writeFile, readFile,
} = require('./file_utils');

(async () => {
  // Get licence file names
  const licences = await getLicencesNames()
  // Extract each licence's type
  const licenceTypes = getFileType(licences)

  // Query the user
  const { file } = await prompts(
    [
      {
        type: 'autocomplete',
        name: 'file',
        message: 'What type of licence file do you need?',
        choices: licenceTypes,
        min: 1,
        limit: 15,
      },
    ],
  )

  if (!file) {
    console.info('👋 No type selected, exiting.')
    return
  }

  // Get licence content based on the answer
  const licenceContent = await readFile(`${file}`)

  // Create the licence file
  await writeFile('LICENSE.md', licenceContent)
})();
